
drop table if exists CONTRACT;
drop table if exists PROSPECT;
drop table if exists CLIENT;

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table CLIENT 
(  
   "id"                   serial                         not null, 
   "businessName"         varchar(150)                   not null, 
   "domain"               varchar(7)                        not null, 
   "adressStreetNumber"   integer                        not null, 
   "adressStreetName"     varchar(200)                   not null, 
   "adressZipCode"        char(7)                        not null, 
   "adressCity"           varchar(100)                   not null, 
   "phoneNumber"          char(20)                       not null, 
   "email"                varchar(200)                   not null, 
   "comments"             varchar(500)                   null, 
   "turnover"             integer                        not null, 
   "headcount"            integer                        not null, 
   constraint PK_CLIENT primary key ("id")
);

/*==============================================================*/
/* Table : PROSPECT                                             */
/*==============================================================*/
create table PROSPECT 
(  
   "id"                   serial                         not null, 
   "businessName"         varchar(150)                   not null, 
   "domain"               varchar(7)                        not null, 
   "adressStreetNumber"   integer                        not null, 
   "adressStreetName"     varchar(200)                   not null, 
   "adressZipCode"        char(7)                        not null, 
   "adressCity"           varchar(100)                   not null, 
   "phoneNumber"          char(20)                       not null, 
   "email"                varchar(200)                   not null, 
   "comments"             varchar(500)                   null, 
   "prospectionDate"      date                       not null, 
   "interested"           boolean                        not null, 
   constraint PK_PROSPECT primary key ("id")
);

/*==============================================================*/
/* Table : CONTRACT                                             */
/*==============================================================*/
create table CONTRACT 
(
   "idContract"           serial                         not null, 
   "id"                   serial                         not null, 
   "titleContract"        varchar(100)                   not null, 
   "descriptionContract"  varchar(500)                   not null, 
   "startDateContract"    date                           not null, 
   "endDateContract"      date                           null, 
   constraint PK_CONTRACT primary key ("idContract"), 
   constraint FK_CONTRACT_ASSOCIATION_CLIENT foreign key ("id")
   references CLIENT
);

-- truncate CONTRACT;
-- truncate PROSPECT;
-- truncate CLIENT;

/*==============================================================*/
/* Insert : CLIENT                                              */
/*==============================================================*/
Insert into CLIENT ("id", "businessName", "domain", "adressStreetNumber", "adressStreetName", "adressZipCode", "adressCity", "phoneNumber", "email", "comments", "turnover", "headcount") values (1, 'Blackberry inc.', 'PRIVATE',  14, 'Rue des programmer',    '67800', 'Bischheim',   '0388675420', 'bb@erry.fr',     null,                               540010,  250);
Insert into CLIENT ("id", "businessName", "domain", "adressStreetNumber", "adressStreetName", "adressZipCode", "adressCity", "phoneNumber", "email", "comments", "turnover", "headcount") values (2, 'Appel',           'PRIVATE',  25, 'Rue Java',              '57500', 'Saint-Avold', '0357523221', 'appel@appel.com', 'Rien a voir avec Appel Etat Unis', 25000,   3);
Insert into CLIENT ("id", "businessName", "domain", "adressStreetNumber", "adressStreetName", "adressZipCode", "adressCity", "phoneNumber", "email", "comments", "turnover", "headcount") values (3, 'logoHeart',       'PRIVATE',  3,  'Rue des investisseurs', '54200', 'Luneville',   '0354252336', 'lh@log.com',      'Gros investissements à venir',     3000,    5);
Insert into CLIENT ("id", "businessName", "domain", "adressStreetNumber", "adressStreetName", "adressZipCode", "adressCity", "phoneNumber", "email", "comments", "turnover", "headcount") values (4, 'CNRS',            'PUBLIC', 18, 'Impasse des bleu',      '54000', 'Nancy',       '0354541020', 'cnrs@cnrs.fr',    null,                               1000000, 3500);         

/*==============================================================*/
/* Insert : PROSPECT                                            */
/*==============================================================*/
Insert into PROSPECT ("id", "businessName", "domain", "adressStreetNumber", "adressStreetName", "adressZipCode", "adressCity", "phoneNumber", "email", "comments", "prospectionDate", "interested") values(1,'CreaLog',        'PRIVATE',  127, 'Boulevard de la Faillite', '57800', 'Forbach', '0387871254', 'cs@crealog.fr',      'travail avec le concurrent', '28/02/2019', TRUE);
Insert into PROSPECT ("id", "businessName", "domain", "adressStreetNumber", "adressStreetName", "adressZipCode", "adressCity", "phoneNumber", "email", "comments", "prospectionDate", "interested") values(2,'Ville de Nancy', 'PUBLIC', 2,   'Avenue des nains',         '54000', 'Nancy',   '0354994500', 'nancycity@nancy.fr', null,                         '14/07/2017', FALSE);

/*==============================================================*/
/* Insert : CONTRACT                                            */
/*==============================================================*/
insert into CONTRACT ("idContract", "id", "titleContract", "descriptionContract", "startDateContract", "endDateContract") values (1, 3, 'Maintenance Apps',     'maintenance des applications desktop des gestion des stocks et crm', '28/01/2019', '31/12/2025');
insert into CONTRACT ("idContract", "id", "titleContract", "descriptionContract", "startDateContract", "endDateContract") values (2, 2, 'Appel dataBase',       'Creation de la base de donnees du client',                           '12/02/2020', null);
insert into CONTRACT ("idContract", "id", "titleContract", "descriptionContract", "startDateContract", "endDateContract") values (3, 4, 'ERP_CNRS',             'Creation ERP pour touts les sites du CNRS',                          '10/05/2016', '20/03/2019');
insert into CONTRACT ("idContract", "id", "titleContract", "descriptionContract", "startDateContract", "endDateContract") values (4, 4, 'Maintenance_ERP_CNRS', 'Maintenance ERP du CNRS',                                            '01/04/2019', null);

Alter sequence CLIENT_id_seq restart with 10;
Alter sequence PROSPECT_id_seq restart with 50;