package fr.afpa.pompey.cda02.ecf01.reverso.Percistance;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class DAOProspect {

    /* si l'on veut pouvoir écrire dans le log il faut le déclarer en static,
 cela nous permettra aussi d'acceder aux outils de connexion */
    private static final Logger LOGGER =
            Logger.getLogger(ConnectionManager.class.getName());

    // pour convertir le String prospection date en date
    public static final DateFormat frenchDateFormat =
            new SimpleDateFormat("dd/MM/yyyy");


    public List<Prospect> findAll() throws ReversoException {

        List <Prospect> prospectList = new ArrayList<>();

        String findAllProspect =
                "select \n" +
                        "\"id\",\n " +
                        "\"businessName\",\n " +
                        "\"domain\",\n " +
                        "\"adressStreetNumber\",\n " +
                        "\"adressStreetName\"\n, " +
                        "\"adressZipCode\",\n" +
                        "\"adressCity\",\n "+
                        "\"phoneNumber\",\n" +
                        "\"email\",\n" +
                        "\"comments\",\n " +
                        "\"prospectionDate\",\n " +
                        "\"interested\"\n " +
                        "from PROSPECT";

        try (Connection connection = ConnectionManager.getConnection();
             // Stateent permet d'envoyer nos requêtes SQL à la base de données, on l'optient grâce à connexion ci dessus
             PreparedStatement statement = connection.prepareStatement(findAllProspect)
        ){
            // System.out.println("Connexion established find All");
            // on chercjhe le résultat d'une requête sur une table, enregistrement par enregistrement,
            // executeQuerry nous permet de renvoyer le résultat de la requête dans l'objet results
            LOGGER.info("Fecht all prospect : " + findAllProspect);
            ResultSet results = statement.executeQuery();

            while (results.next()){
                Prospect prospect = new Prospect ();
                prospect.setId(results.getInt(1));
                prospect.setBusinessName(results.getString(2));
                prospect.setDomain(Company.Domain.valueOf(results.getString(3)));
                prospect.setAddressStreetNumber(results.getInt(4));
                prospect.setAddressStreetName(results.getString(5));
                prospect.setAddressZipCode(results.getString(6));
                prospect.setAddressCity(results.getString(7));
                prospect.setPhoneNumber(results.getString(8));
                prospect.setEmail(results.getString(9));
                prospect.setComments(results.getString(10));
                prospect.setProspectionDate(frenchDateFormat.format(results.getDate(11)));
                prospect.setInterested(results.getBoolean(12));
                prospectList.add(prospect);
            }
            System.out.println(findAllProspect);

        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        return prospectList;
    }

    public Prospect find(int id) throws ReversoException {

        Prospect prospect = null;

        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "select * from PROSPECT where id =" +id)
        ){
            //System.out.println("Connexion established find by id");
            // Le logger nous permet d'écrire un message, équivaut à un system.out.println mais avec une gestion
            // de la gravité du message
            LOGGER.info("Fetch prospect by id : " + "select * from PROSPECT where id = "+id);
            ResultSet results = statement.executeQuery();

            // avec reluts.next on se déplace à chaque fois au prochain enregistrement
            if (results.next()) {
                prospect = new Prospect();
            }

            prospect.setId(results.getInt(1));
            prospect.setBusinessName(results.getString(2));
            prospect.setDomain(Company.Domain.valueOf(results.getString(3)));
            prospect.setAddressStreetNumber(results.getInt(4));
            prospect.setAddressStreetName(results.getString(5));
            prospect.setAddressZipCode(results.getString(6));
            prospect.setAddressCity(results.getString(7));
            prospect.setPhoneNumber(results.getString(8));
            prospect.setEmail(results.getString(9));
            prospect.setComments(results.getString(10));
            prospect.setProspectionDate(frenchDateFormat.format(results.getDate(11)));
            prospect.setInterested(results.getBoolean(12));

        }
        catch (SQLException sqle){
            throw new ReversoException(sqle);
        }
        return prospect;
    }

    public boolean insert (Prospect prospect) throws ReversoException {

        /* On vérifie d'abord si le prospect existe déjà ? */
        if (prospect.getId() != null){
            // lance une exception si l'id du prospect n'est pas null
            throw new UnsupportedOperationException(
                    "The prospect already has an id and shouldn' be inserted");
        }

        boolean sucess = true;

        String insertProspect = "insert into \"prospect\" \n " +
                "(\n" +
                "\"businessName\",\n" +
                "\"domain\",\n" +
                "\"adressStreetNumber\",\n" +
                "\"adressStreetName\",\n" +
                "\"adressZipCode\",\n" +
                "\"adressCity\",\n" +
                "\"phoneNumber\",\n" +
                "\"email\",\n" +
                "\"comments\",\n" +
                "\"prospectionDate\",\n" +
                "\"interested\"\n"+
                ")\n" +
                "values\n" +
                "(\n" +
                "'" + prospect.getBusinessName() + "', \n" +
                "'" + prospect.getDomain().name() +"', \n" +
                "" + prospect.getAddressStreetNumber() +", \n" +
                "'" + prospect.getAddressStreetName() +"', \n" +
                "'" + prospect.getAddressZipCode()+"', \n" +
                "'" + prospect.getAddressCity()+"', \n" +
                "'" + prospect.getPhoneNumber()+"', \n" +
                "'" + prospect.getEmail()+"', \n" +
                ""  + (prospect.getComments() == null ? "null": "'" + prospect.getComments() +"'")+ ",\n" +
                //équivaut au STR_TO_DATE de mySQL, permet de modifier la valeur String reçu en format date
                "   to_date('" + prospect.getProspectionDate()+"','DD/MM/YYYY'), \n" +
                "" + prospect.isInterested()+" \n" +
                ")";

        Prospect.getList().add(prospect);

        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     insertProspect,
                     // Genrated Keys, nous génère automatiquement l'id
                     PreparedStatement.RETURN_GENERATED_KEYS))
        {
            /* traitement des transaction (rappel une transaction permet de valider un esemble de traitementrs
            de la base de données que si ils sont tous effectués correctement. On gère la transaction à partir
            de l'objet connection, par défaut chaque connection est un auto-commit validée unitairement
            (donc 1 traitement = 1 transaction), pour valider plusieurs traitement dans une même transaction,
            on désactive setAutCommit */
            connection.setAutoCommit(false);

            LOGGER.info("Insert Prospect with: " + insertProspect);
            // affectedRows permeet de renvoyer un entier à chaque fois qiue l'Update est effectuer
            // executeUpdate retourne le nombre d'enregistrement qui on été mis à jours.
            int affectedRows = statement.executeUpdate();

            // pour générer la clé primaire (idClient) de manière automatique
            Integer generatedId = null;
            ResultSet generatedKeys = statement.getGeneratedKeys();

            // vérifie que l'on est passer à la clé suivante et que au moins une ligne de la BD est affectée
            if (!generatedKeys.next() || affectedRows <1)
            {
                sucess = false;
            }
            else {
                // on, incrémente de la clé suivante (équivaut à id +1)
                generatedId = generatedKeys.getInt("id");
            }
            prospect.setId(generatedId);
            // l'appel de la méthode commit premet de valider la transaction courante, et passe donc à une nouvelle transaction
            connection.commit();
            // une fois toutes faites elles sont rassemblés et renvoyé à la base de données en même temps
            connection.setAutoCommit(true);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        return sucess;
    }

    public boolean update (Prospect prospect) throws ReversoException {

        /* On vérifie d'abord si le prospect n'existe pas déjà ? */
        if (prospect.getId() == null) {
            throw new UnsupportedOperationException(
                    "The prospect has no id and shouldn' be inserted first");
        }

        boolean sucess = true;

        String updateProspect=
                " update \"prospect\" set\n" +
                        "\"businessName\" = '" + prospect.getBusinessName() + "', \n" +
                        "\"domain\" = '" + prospect.getDomain().name() + "', \n" +
                        "\"adressStreetNumber\" = " + prospect.getAddressStreetNumber() + ", \n" +
                        "\"adressStreetName\" = '" + prospect.getAddressStreetName() + "', \n" +
                        "\"adressZipCode\" = '" + prospect.getAddressZipCode() + "', \n" +
                        "\"adressCity\" = '" + prospect.getAddressCity() + "', \n" +
                        "\"phoneNumber\" = '" + prospect.getPhoneNumber() + "', \n" +
                        "\"email\" = '" + prospect.getEmail() + "', \n" +
                        // si il est null on lui passe null, sinon on entre le texte
                        "\"comments\" = " + (prospect.getComments() == null ? "null": "'" + prospect.getComments() +"'")+ ",\n" +
                        // permet la convertion de la valeur reçu en string au format date désiré
                        "\"prospectionDate\" =  to_date('" + prospect.getProspectionDate() + "','DD/MM/YYYY'), \n" +
                        "\"interested\" = "+ prospect.isInterested() + " \n" +
                        " where \"id\" = " + prospect.getId() ;

        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(updateProspect))
        {
            connection.setAutoCommit(false);

            LOGGER.info("Update Prospect with: " + updateProspect);
            // permeet de renvoyer un entier à chaque fois qiue l'Update est effectuer
            int affectedRows = statement.executeUpdate();

            // vérifie  que au moins une ligne de la BD est affectée
            if (affectedRows <1)
            {
                sucess = false;
            }

            connection.commit();
            connection.setAutoCommit(true);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        return sucess;
    }

    public boolean save (Prospect prospect) throws ReversoException {
        if (prospect.getId() == null){
            return this.insert(prospect);
        }
        else {
            return this.update(prospect);
        }
    }

    public boolean delete (Prospect prospect) throws ReversoException {

        boolean sucess = true;

        if (prospect.getId() == null){
            throw  new UnsupportedOperationException(
                    "The client has not id and should be inserted first");
        }

        String deleteProspect = "delete from \"prospect\" where \"id\" = " + prospect.getId();

        try (Connection connection = ConnectionManager.getConnection();
             // on lance notre requête de delete dans la base de données
             PreparedStatement statement = connection.prepareStatement(deleteProspect))
        {
            // pour rassembler les différentes supressions éventuelles
            connection.setAutoCommit(false);

            // on définit un entier sur la variable affectedrows, qui nous permettra de vérifier que la ligne
            // selectionnée dans la base de donnée est bien supprimée
            int affectedRows;

            // on execute la mise à jour de la modification dans la base de données, donc la supression.
            affectedRows = statement.executeUpdate();

            // vérifie que l'opération à été effectuer avec succès.
            sucess = affectedRows>1;

            Prospect.getList().remove(affectedRows);

            // valide la transaction
            connection.commit();
            // valide les transactions
            connection.setAutoCommit(true);
            prospect.setId(null);
        }
        catch (SQLException sqle){
            throw new ReversoException(sqle);
        }

        return sucess;
    }

}
