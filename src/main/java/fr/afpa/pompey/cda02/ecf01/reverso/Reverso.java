/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.pompey.cda02.ecf01.reverso;

import fr.afpa.pompey.cda02.ecf01.reverso.Percistance.DAOClient;
import fr.afpa.pompey.cda02.ecf01.reverso.Percistance.DAOProspect;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;
import fr.afpa.pompey.cda02.ecf01.reverso.swing.HomeFrame;

import java.util.List;


public class Reverso {

    public static void loadTestData() throws ReversoException {

        Company[] companies = {
            new Client(
                "AFPA Pompey",
                Company.Domain.PUBLIC, 
                5, 
                "square Eugene Herzog",
                "54340", 
                "Frouard", 
                "0356981452",
                "contact@pompey.afpa.fr", 
                null, 
                250000, 
                10
            ),
            new Client(
                "Feel Good Inc.", 
                Company.Domain.PRIVATE, 
                151, 
                "Real del Milciades", 
                "33698", 
                "Frouard", 
                "03 89 44 56 89", 
                "info@feelgoodinc.com", 
                "Kilroy was here", 
                7580000, 
                112
            ),
            new Client(
                "Skyndu", 
                Company.Domain.PRIVATE, 
                6228, 
                "Anderson street", 
                "25636", 
                "Francisco Villa", 
                "7084417317", 
                "mboothman0@friendfeed.com", 
                null, 
                36504500, 
                220
            ),
            new Prospect(
                "Shin R.A.", 
                Company.Domain.PUBLIC, 
                967, 
                "square Eugène Herzog", 
                "54340", 
                "Nkandla", 
                "03 24 42 68 61", 
                "tolynno@bandcamp.com", 
                "No comment", 
                "13/09/2019", 
                true
            ),
            new Prospect(
                "Gabcube", 
                Company.Domain.PRIVATE, 
                9, 
                "Riachão das Neves", 
                "36408", 
                "Várzea Grande", 
                "+331 25 72 50 16", 
                "mmitkova@addtoany.com", 
                null, 
                "16/09/2016", 
                false
            ),
            new Prospect(
                "Jabbersphere", 
                Company.Domain.PRIVATE, 
                87, 
                "rue de Montfermeil", 
                "83308", 
                "Casalinho", 
                "+33 4 87 46 07 57", 
                "bcottah@wufoo.com", 
                "Klaatu barada nikto", 
                "06/05/2018", 
                true
            )
        };
        
        for (Company company: companies) {
            Company.getList().add(company);
        }
    }

    public static void main(String[] args) throws ReversoException {

        DAOClient daoClient = new DAOClient();
        DAOProspect daoProspect = new DAOProspect();

        // On ajoute les clients trouvé dans la base de données dans notre liste client et prospect, ce qui permettra
        // de les visualiser dans la Frame
        Company.getList().addAll(daoClient.findAll());
        Company.getList().addAll(daoProspect.findAll());

        Reverso app = new Reverso();
        app.start();

    }
    
    public void start() {
        HomeFrame home = new HomeFrame();
        home.setVisible(true);
    }

}