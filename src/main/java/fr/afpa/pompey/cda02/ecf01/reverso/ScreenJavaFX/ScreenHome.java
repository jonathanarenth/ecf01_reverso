package fr.afpa.pompey.cda02.ecf01.reverso.ScreenJavaFX;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class ScreenHome extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(
                getClass().getClassLoader().getResource("fxml/ScreenHome.fxml"));
        primaryStage.setTitle("Reverso Application");
        primaryStage.setScene(new Scene(root, 800,500));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
