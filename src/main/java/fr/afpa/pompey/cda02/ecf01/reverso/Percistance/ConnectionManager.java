package fr.afpa.pompey.cda02.ecf01.reverso.Percistance;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

public class ConnectionManager {

    /* instancier un objet Logger pour pour éviter les occurence de System.out.printlin(),
     il gardera une trace des évènements dans le terminal
     il importera une llibrairie Logger
     */
    private static final Logger LOGGER =
            Logger.getLogger(ConnectionManager.class.getName());

    /* Méthode static qui produit la connexion pour nous */
    public static Connection getConnection () throws SQLException {

        /* On créer l'objet properties qui nous permmettra d'apeller plus tard la méthode load() qui elle accepte
        soit un Reader soit un InputStream (que nous allons utiliser)*/

        Properties databaseProperties = new Properties();

        /* On va chercher les éléments de connexion présent dans le fichier resources /* database.properties */
        try (InputStream input =
                     ConnectionManager
                             .class
                             .getClassLoader()
                             .getResourceAsStream("database.properties")
        ){
            databaseProperties.load(input);
        } catch (IOException ieo){
        //On désigne donc ici avec le Logger que l'erreur est particulièrement grave
            LOGGER.severe("database properties files couldn't be loaded: " + ieo.getMessage());
        }
        /* On retourne la méthode DriverManager.getConnection() qui nous permet maintenant de lui passer
        un objet Properties, on a fait celà car ça permet de rendre impossible de modifier les informations
        de connexion sans recompiler l'application et d'intervetir ce fichier avec un autre */
        return DriverManager.getConnection(
                databaseProperties.getProperty("url"), databaseProperties);
    }
}
