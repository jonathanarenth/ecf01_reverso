package fr.afpa.pompey.cda02.ecf01.reverso.Percistance;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class DAOClient {

    /* si l'on veut pouvoir écrire dans le log il faut le déclarer en static,
     cela nous permettra aussi d'acceder aux outils de connexion */
    private static final Logger LOGGER =
            Logger.getLogger(ConnectionManager.class.getName());


    public List<Client> findAll() throws ReversoException {

        List <Client> clientList = new ArrayList<>();

        try (Connection connection = ConnectionManager.getConnection();
             // Stateent permet d'envoyer nos requêtes SQL à la base de données, on l'optient grâce à connexion ci dessus
             PreparedStatement statement = connection.prepareStatement(
                     "select \n" +
                             "\"id\",\n " +
                             "\"businessName\",\n " +
                             "\"domain\",\n " +
                             "\"adressStreetNumber\",\n " +
                             "\"adressStreetName\"\n, " +
                             "\"adressZipCode\",\n" +
                             "\"adressCity\",\n "+
                             "\"phoneNumber\",\n" +
                             "\"email\",\n" +
                             "\"comments\",\n " +
                             "\"turnover\",\n " +
                             "\"headcount\"\n " +
                             "from CLIENT"
             )
        ){
           // System.out.println("Connexion established find All");
            // on chercjhe le résultat d'une requête sur une table, enregistrement par enregistrement,
            // executeQuerry nous permet de renvoyer le résultat de la requête dans l'objet results
            ResultSet results = statement.executeQuery();

            while (results.next()){
                Client c = new Client ();
                c.setId(results.getInt(1));
                c.setBusinessName(results.getString(2));
                c.setDomain(Company.Domain.valueOf(results.getString(3)));
                c.setAddressStreetNumber(results.getInt(4));
                c.setAddressStreetName(results.getString(5));
                c.setAddressZipCode(results.getString(6));
                c.setAddressCity(results.getString(7));
                c.setPhoneNumber(results.getString(8));
                c.setEmail(results.getString(9));
                c.setComments(results.getString(10));
                c.setTurnover(results.getInt(11));
                c.setHeadcount(results.getInt(12));
                clientList.add(c);
            }

//            Client.getList().addAll(clientList);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        return clientList;
    }

   public Client find(int id) throws ReversoException {

        Client client = null;

        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
            "select * from CLIENT where id =" +id)
        ){
        //System.out.println("Connexion established find by id");
            // Le logger nous permet d'écrire un message, équivaut à un system.out.println mais avec une gestion
            // de la gravité du message
            LOGGER.info("Fetch client by id : " + "select * from CLIENT where id = "+id);
            ResultSet results = statement.executeQuery();

            // avec reluts.next on se déplace à chaque fois au prochain enregistrement
            if (results.next()) {
            client = new Client();
            }

            client.setId(results.getInt(1));
            client.setBusinessName(results.getString(2));
            client.setDomain(Company.Domain.valueOf(results.getString(3)));
            client.setAddressStreetNumber(results.getInt(4));
            client.setAddressStreetName(results.getString(5));
            client.setAddressZipCode(results.getString(6));
            client.setAddressCity(results.getString(7));
            client.setPhoneNumber(results.getString(8));
            client.setEmail(results.getString(9));
            client.setComments(results.getString(10));
            client.setTurnover(results.getInt(11));
            client.setHeadcount(results.getInt(12));

    }
        catch (SQLException sqle){
            throw new ReversoException(sqle);
    }
       return client;
   }

    public boolean insert (Client client) throws ReversoException {

        /* On vérifie d'abord si le client existe déjà ? */
        if (client.getId() != null){
            // lance une exception si l'id du client n'est pas null
            throw new UnsupportedOperationException(
                    "The client already has an id and shouldn' be inserted");
        }

        boolean sucess = true;

        String insertClient = "insert into \"client\" \n " +
                "(\n" +
                "\"businessName\",\n" +
                "\"domain\",\n" +
                "\"adressStreetNumber\",\n" +
                "\"adressStreetName\",\n" +
                "\"adressZipCode\",\n" +
                "\"adressCity\",\n" +
                "\"phoneNumber\",\n" +
                "\"email\",\n" +
                "\"comments\",\n" +
                "\"turnover\",\n" +
                "\"headcount\"\n"+
                ")\n" +
                "values\n" +
                "(\n" +
                "'" + client.getBusinessName() + "', \n" +
                "'" + client.getDomain().name() +"', \n" +
                "" + client.getAddressStreetNumber() +", \n" +
                "'" + client.getAddressStreetName() +"', \n" +
                "'" + client.getAddressZipCode()+"', \n" +
                "'" + client.getAddressCity()+"', \n" +
                "'" + client.getPhoneNumber()+"', \n" +
                "'" + client.getEmail()+"', \n" +
                "" + (client.getComments() == null ? "null": "'" + client.getComments() +"'")+ ",\n" +
                "" + client.getTurnover()+", \n" +
                "" + client.getHeadcount()+" \n" +
                ")";

        Client.getList().add(client);

        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     insertClient,
                     // Genrated Keys, nous génère automatiquement l'id
                     PreparedStatement.RETURN_GENERATED_KEYS))
        {
            /* traitement des transaction (rappel une transaction permet de valider un esemble de traitementrs
            de la base de données que si ils sont tous effectués correctement. On gère la transaction à partir
            de l'objet connection, par défaut chaque connection est un auto-commit validée unitairement
            (donc 1 traitement = 1 transaction), pour valider plusieurs traitement dans une même transaction,
            on désactive setAutCommit */
            connection.setAutoCommit(false);

            LOGGER.info("Insert Client with: " +insertClient);
            // affectedRows permeet de renvoyer un entier à chaque fois qiue l'Update est effectuer
            // executeUpdate retourne le nombre d'enregistrement qui on été mis à jours.
            int affectedRows = statement.executeUpdate();

            // pour générer la clé primaire (idClient) de manière automatique
            Integer generatedId = null;
            ResultSet generatedKeys = statement.getGeneratedKeys();

            // vérifie que l'on est passer à la clé suivante et que au moins une ligne de la BD est affectée
            if (!generatedKeys.next() || affectedRows <1)
            {
                sucess = false;
            }
            else {
                // on, incrémente de la clé suivante (équivaut à id +1)
                generatedId = generatedKeys.getInt("id");
            }
            client.setId(generatedId);
            // l'appel de la méthode commit premet de valider la transaction courante, et passe donc à une nouvelle transaction
            connection.commit();
            // une fois toutes faites elles sont rassemblés et renvoyé à la base de données en même temps
            connection.setAutoCommit(true);
        }
            catch (SQLException sqle) {
                throw new ReversoException(sqle);
        }
        return sucess;
    }

    public boolean update (Client client) throws ReversoException {

        /* On vérifie d'abord si le client n'existe pas déjà ? */
        if (client.getId() == null) {
            throw new UnsupportedOperationException(
                    "The client has no id and shouldn' be inserted first");
        }

        boolean sucess = true;

        String updateClient=
                " update \"client\" set\n" +
                        "\"businessName\" = '" + client.getBusinessName() + "', \n" +
                        "\"domain\" = '" + client.getDomain().name() + "', \n" +
                        "\"adressStreetNumber\" = " + client.getAddressStreetNumber() + ", \n" +
                        "\"adressStreetName\" = '" + client.getAddressStreetName() + "', \n" +
                        "\"adressZipCode\" = '" + client.getAddressZipCode() + "', \n" +
                        "\"adressCity\" = '" + client.getAddressCity() + "', \n" +
                        "\"phoneNumber\" = '" + client.getPhoneNumber() + "', \n" +
                        "\"email\" = '" + client.getEmail() + "', \n" +
                        // si il est null on lui passe null, sinon on entre le texte
                        "\"comments\" = " + (client.getComments() == null ? "null": "'" + client.getComments() +"'")+ ",\n" +
                        "\"turnover\" = " + client.getTurnover() + ", \n" +
                        "\"headcount\" = "+ client.getHeadcount() + " \n" +
                        " where \"id\" = " + client.getId() ;

        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(updateClient))
        {
            connection.setAutoCommit(false);

            LOGGER.info("Update Client with: " + updateClient);
            // permeet de renvoyer un entier à chaque fois qiue l'Update est effectuer
            int affectedRows = statement.executeUpdate();

            // vérifie  que au moins une ligne de la BD est affectée
            if (affectedRows <1)
            {
                sucess = false;
            }

            connection.commit();
            connection.setAutoCommit(true);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        return sucess;
    }

    public boolean save (Client client) throws ReversoException {
        if (client.getId() == null){
            return this.insert(client);
        }
        else {
            return this.update(client);
        }
    }

    public boolean delete (Client client) throws ReversoException {

        boolean sucess = true;

        if (client.getId() == null){
            throw  new UnsupportedOperationException(
                    "The client has not id and should be inserted first");
        }

        String deleteClient = "delete from \"client\" where \"id\" = " + client.getId();

        try (Connection connection = ConnectionManager.getConnection();
             // on lance notre requête de delete dans la base de données
             PreparedStatement statement = connection.prepareStatement(deleteClient))
        {
            // pour rassembler les différentes supressions éventuelles
            connection.setAutoCommit(false);

            // on définit un entier sur la variable affectedrows, qui nous permettra de vérifier que la ligne
            // selectionnée dans la base de donnée est bien supprimée
            int affectedRows;

            // on execute la mise à jour de la modification dans la base de données, donc la supression.
            affectedRows = statement.executeUpdate();

            // vérifie que l'opération à été effectuer avec succès.
            sucess = affectedRows>1;

            Client.getList().remove(affectedRows);

            // valide la transaction
            connection.commit();
            // valide les transactions
            connection.setAutoCommit(true);
            client.setId(null);
        }
        catch (SQLException sqle){
            throw new ReversoException(sqle);
        }

        return sucess;
    }

}
