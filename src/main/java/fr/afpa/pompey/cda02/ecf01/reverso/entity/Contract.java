package fr.afpa.pompey.cda02.ecf01.reverso.entity;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;

import java.time.*;

public class Contract {

    private static int counterContract =1;
    private int idContract;
    private String titleContract;
    private String descriptionContract;
    private LocalDate startDateContract;
    private LocalDate endDateContract;

    /**
     *  A default Contract constructor
     */
    public Contract (){
        super();
        this.idContract = Contract.counterContract++;
    }

    /**
     * Every values is required except endDateContract
     *
     * @param idContract
     * @param titleContract
     * @param descriptionContract
     * @param startDateContract
     * @param endDateContract
     * @throws ReversoException
     */
    public Contract(
            int idContract,
            String titleContract,
            String descriptionContract,
            LocalDate startDateContract,
            LocalDate endDateContract
    ) throws ReversoException
    {
        this.idContract = Contract.counterContract++;
        this.setTitleContract(titleContract);
        this.setDescriptionContract(descriptionContract);
        this.setStartDateContract(startDateContract);
        this.setEndDateContract(endDateContract);
    }


    public String getTitleContract() {
        return titleContract;
    }
    public String getDescriptionContract() {
        return descriptionContract;
    }
    public LocalDate getStartDateContract() {
        return startDateContract;
    }
    public LocalDate getEndDateContract() {
        return endDateContract;
    }

    /**
     * The title contract cannot be null or empty and should contain
     * more than whitespaces.
     *
     * @param titleContract
     * @throws ReversoException
     */
    public void setTitleContract(String titleContract) throws ReversoException {
        if (titleContract == null || titleContract.trim().isEmpty()){
            throw new ReversoException
                    ("Contract title cannot be empty (received '" + titleContract + "'");
        }
        this.titleContract = titleContract;
    }

    /**
     * The description contract cannot be null or empty and should contain
     * more than whitespaces.
     *
     * @param descriptionContract
     * @throws ReversoException
     */
    public void setDescriptionContract(String descriptionContract) throws ReversoException {
        if (descriptionContract == null || descriptionContract.trim().isEmpty()){
            throw new ReversoException
                    ("Contract description cannot be empty (received '" + descriptionContract + "'");
        }
        this.descriptionContract = descriptionContract;
    }

    /**
     * The start date contract must not be null
     *
     * @param startDateContract
     * @throws ReversoException
     */
    public void setStartDateContract(LocalDate startDateContract) throws ReversoException {
        if (startDateContract == null){
            throw new ReversoException("Start date contract cannot be null");
        }
        this.startDateContract = startDateContract;
    }

    /**
     * The End date Contract maybe empty or null
     *
     * @param endDateContract
     */
    public void setEndDateContract(LocalDate endDateContract){
            this.endDateContract = endDateContract;
    }

    public String toString (){
        return "Contract{"
                + "id Contract =" + idContract
                + ", title contract =" + titleContract
                + ", description contract =" + descriptionContract
                + ", start date contract =" + startDateContract
                + ", end date contract =" + endDateContract
                + "}";
    }
}
