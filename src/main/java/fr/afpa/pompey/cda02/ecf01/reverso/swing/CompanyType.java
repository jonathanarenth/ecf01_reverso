package fr.afpa.pompey.cda02.ecf01.reverso.swing;


import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;


public enum CompanyType {
    CLIENT(Client.class),
    PROSPECT(Prospect.class);


    private Class equivalentClass;

    CompanyType(Class equivalentClass) {
        this.equivalentClass = equivalentClass;
    }

    public Class getEquivalentClass() {
        return this.equivalentClass;
    }
}