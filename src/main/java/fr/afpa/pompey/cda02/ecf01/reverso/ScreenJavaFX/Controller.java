/**
 * Sample Skeleton for 'fxml_reversoApplication.fxml' Controller Class
 */

package fr.afpa.pompey.cda02.ecf01.reverso.ScreenJavaFX;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

  @FXML // fx:id="panelChoiseCompanyType"
  private Pane panelChoiseCompanyType; // Value injected by FXMLLoader

  @FXML // fx:id="btnClient"
  private Button btnClient; // Value injected by FXMLLoader

  @FXML // fx:id="btnProspect"
  private Button btnProspect; // Value injected by FXMLLoader

  @FXML // fx:id="panelActionOnCompany"
  private Pane panelActionOnCompany; // Value injected by FXMLLoader

  @FXML // fx:id="btnDisplay"
  private Button btnDisplay; // Value injected by FXMLLoader

  @FXML // fx:id="BtnUpdate"
  private Button BtnUpdate; // Value injected by FXMLLoader

  @FXML // fx:id="BtnCreate"
  private Button BtnCreate; // Value injected by FXMLLoader

  @FXML // fx:id="btnDelete"
  private Button btnDelete; // Value injected by FXMLLoader

  @FXML // fx:id="panelListCompany"
  private Pane panelListCompany; // Value injected by FXMLLoader

  @FXML // fx:id="listChoiseCompany"
  private ChoiceBox<?> listChoiseCompany; // Value injected by FXMLLoader

  @FXML // fx:id="btnValidation"
  private Button btnValidation; // Value injected by FXMLLoader

  @FXML // fx:id="btnExit"
  private Button btnExit; // Value injected by FXMLLoader

  @FXML // fx:id="btnReturn"
  private Button btnReturn; // Value injected by FXMLLoader

  @FXML // fx:id="titleApplication"
  private Label titleApplication; // Value injected by FXMLLoader
  private ActionEvent event;


  public void selectClient(javafx.event.ActionEvent actionEvent) {
  }

  public void selectProspect(javafx.event.ActionEvent actionEvent) {
  }

  public void displayListCompany(javafx.event.ActionEvent actionEvent) {
  }

  public void UpdateCompany(javafx.event.ActionEvent actionEvent) {
  }

  public void createCompany(javafx.event.ActionEvent actionEvent) {
  }

  public void deleteCompany(javafx.event.ActionEvent actionEvent) {
  }

  public void selectCompany(MouseEvent mouseEvent) {
  }

  public void confirmSelectionCompany(javafx.event.ActionEvent actionEvent) {
  }

  public void exitApplication(javafx.event.ActionEvent actionEvent) {
  }

  public void returnPrincipalScreen(javafx.event.ActionEvent actionEvent) {
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    System.out.println("initialisation ok");
  }
}

