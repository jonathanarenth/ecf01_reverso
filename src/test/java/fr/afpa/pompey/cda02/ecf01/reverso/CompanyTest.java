package fr.afpa.pompey.cda02.ecf01.reverso;

import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CompanyTest {

    public class ConcreteCompany extends Company {}

    @ParameterizedTest
    @DisplayName("Test invalid business name")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "\t",
            "\n"
    })
    void company_business_name_invalid (String company_name){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setBusinessName(company_name);
        });
    }

    @Test
    @DisplayName("Invalid null domain")
    void company_null_domain (){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setDomain(null);
        });
    }

    @ParameterizedTest
    @DisplayName("Test invalid values for adress street number")
    @ValueSource(ints = {
            -5,
            -58795,
            Integer.MIN_VALUE
    })
    void invalid_adressStreet_number_company (int company_adressStreet_number ){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setAddressStreetNumber(company_adressStreet_number);
        });
    }

    @ParameterizedTest
    @DisplayName("Test valid values for adress street number")
    @ValueSource(ints = {
            5,
            58795,
            Integer.MAX_VALUE
    })
    void valid_adressStreet_number_company (int company_adressStreet_number ){
        ConcreteCompany company = new ConcreteCompany();
        assertDoesNotThrow(() -> {
            company.setAddressStreetNumber(company_adressStreet_number);
        });
    }

    @ParameterizedTest
    @DisplayName("Test invalid adress street name")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "\t",
            "\n"
    })
    void invalid_adressStreet_name (String adressStreet_name){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setAddressStreetName(adressStreet_name);
        });
    }

    @ParameterizedTest
    @DisplayName("Test invalid zip code")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "\t",
            "\n"
    })
    void invalid_zipCode (String zipCode){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setAddressZipCode(zipCode);
        });
    }

    @ParameterizedTest
    @DisplayName("Test invalid adress city")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "\t",
            "\n"
    })
    void invalid_adress_city(String adress_city){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setAddressCity(adress_city);
        });
    }
    @ParameterizedTest
    @DisplayName("Test invalid phone number")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "0001020304050607",
            "01-02-03-04-05-",
            "01-02-03-04-05",
            "01/02/03/04/05",
            "010230405",
            "0052 21 2213",
            "\t",
            "\n"
    })
    void invalid_phoneNumber (String phoneNumber){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setPhoneNumber(phoneNumber);
        });
    }

    @ParameterizedTest
    @DisplayName("Test valid phone number")
    @ValueSource(strings = {
            "0123456789",
            "01 23 45 67 89",
            "+001 23 45 67 89",
            "+00 1 23 45 67 89",
            "0102030405              ",
            " 0102030405"
    })
    void valid_phoneNumber (String phoneNumber){
        ConcreteCompany company = new ConcreteCompany();
        assertDoesNotThrow(() -> {
            company.setPhoneNumber(phoneNumber);
        });
    }

    @ParameterizedTest
    @DisplayName("Test invalid phone number")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "g.g@a.fr@",
            "+gdgdh@gfgf.com",
            "ggfhdjgdff",
            "@fdgd",
            "@",
            "gfjhf.gfklf",
            "hfhfg@",
            "\t",
            "\n",
    })
    void invalid_email (String email){
        ConcreteCompany company = new ConcreteCompany();
        assertThrows(ReversoException.class, () -> {
            company.setEmail(email);
        });
    }
}
