package fr.afpa.pompey.cda02.ecf01.reverso;

import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ClientTest {

   // @ParameterizedTest
   // @DisplayName("Invalid null domain")
   // @NullSource
   // void client_null_domain (String client_domain){
    //    Client client = new Client();
     //   assertThrows(ReversoException.class, () -> {
     //       client.setDomain(client_domain);
     //   });
  //  }

    @ParameterizedTest
    @DisplayName("An array of invalid values for turnover")
    @ValueSource(ints = {
            -5,
            -58795,
            Integer.MIN_VALUE
    })
    void array_turnover_invalidValues(int turnover) {
        Client client = new Client();
        assertThrows(ReversoException.class, () -> {
            client.setTurnover(turnover);
        });
    }

    @ParameterizedTest
    @DisplayName("Valid values Turnover")
    @ValueSource(ints = {
            2000,
            58795,
            Integer.MAX_VALUE
    })
    void turnover_validValues(int turnover) {
        Client client = new Client();
        assertDoesNotThrow( () -> {
            client.setTurnover(turnover);
        });
    }

    @ParameterizedTest
    @DisplayName("An array of invalid values for headcount")
    @ValueSource(ints = {
            0,
            -5,
            -58795,
            Integer.MIN_VALUE
    })
    void array_headcount_invalidValues(int headcount) {
        Client client = new Client();
        assertThrows(ReversoException.class, () -> {
            client.setHeadcount(headcount);
        });
    }

    @Test
    @DisplayName("Valid value headcount")
    void headcount_validValues() {
        Client client = new Client();
        assertDoesNotThrow( () -> {
            client.setHeadcount(1);
        });
    }

    @ParameterizedTest
    @DisplayName("Invalid divide turnover by headcount")
    @CsvSource({
            "100,100",
            "100,200",
            "100,0"
    })
    void invalid_turnover_headcount_ratio(int turnover, int headcount) {
        Client client = new Client();
        assertThrows(ReversoException.class, () -> {
            client.setTurnover(turnover);
            client.setHeadcount(headcount);
        });
    }

    @ParameterizedTest
    @DisplayName("Valid divide turnover by headcount")
    @CsvSource({
            "10000,100",
            "10000000,200",
            "100,3"
    })
    void valid_turnover_headcount_ratio(int turnover, int headcount) {
        Client client = new Client();
        assertDoesNotThrow(() -> {
            client.setTurnover(turnover);
            client.setHeadcount(headcount);
        });
    }

}



