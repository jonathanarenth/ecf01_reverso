package fr.afpa.pompey.cda02.ecf01.reverso;

import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

public class ProspectTest {

    @ParameterizedTest
    @DisplayName("An array of invalid values for prospectionDate")
    @NullSource
    @EmptySource
    @ValueSource (strings = {
            " ",
            "\t",
            "\n",
            "an invalid value",
            "00-00-0000"
    })
    void array_Of_InvalidValues(String prospectionDate) {
        Prospect prospect = new Prospect();

            assertThrows(ReversoException.class, () -> {
                prospect.setProspectionDate(prospectionDate);
            });
    }

    @ParameterizedTest
    @DisplayName("An array of valid values for prospectionDate")
    @ValueSource (strings = {
            "12/32/2587",
            "12/01/2020",
            "45/28/9999",
    })
    void array_Of_ValidValues(String prospectionDate) {
        Prospect prospect = new Prospect();

        assertDoesNotThrow(() -> {
            prospect.setProspectionDate(prospectionDate);
        });

    }
}
