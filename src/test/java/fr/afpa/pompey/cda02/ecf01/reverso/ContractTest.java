package fr.afpa.pompey.cda02.ecf01.reverso;

import fr.afpa.pompey.cda02.ecf01.reverso.entity.Contract;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ContractTest {

    @ParameterizedTest
    @DisplayName("Test invalid title Contract")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "\t",
            "\n"
    })
    void invalid_title_contrcat (String titleContract){
        Contract contract = new Contract();
        assertThrows(ReversoException.class, () -> {
            contract.setTitleContract(titleContract);
        });
    }

    @ParameterizedTest
    @DisplayName("Test invalid description Contract")
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            " ",
            "\t",
            "\n"
    })
    void invalid_description_contract (String descriptionContract){
        Contract contract = new Contract();
        assertThrows(ReversoException.class, () -> {
            contract.setDescriptionContract(descriptionContract);
        });
    }

    @Test
    @DisplayName("Invalid null start date contract")
    void company_null_domain (){
        Contract contract = new Contract();
        assertThrows(ReversoException.class, () -> {
            contract.setStartDateContract(null);
        });
    }
}
