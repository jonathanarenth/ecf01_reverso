/*==============================================================*/
/* Nom de SGBD :  SAP SQL Anywhere 16                           */
/* Date de cr�ation :  26/02/2020 08:44:07                      */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_CLIENT_HERITAGE__COMPANY') then
    alter table CLIENT
       delete foreign key FK_CLIENT_HERITAGE__COMPANY
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_CONTRACT_ASSOCIATI_CLIENT') then
    alter table CONTRACT
       delete foreign key FK_CONTRACT_ASSOCIATI_CLIENT
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_PROSPECT_HERITAGE__COMPANY') then
    alter table PROSPECT
       delete foreign key FK_PROSPECT_HERITAGE__COMPANY
end if;

drop index if exists CLIENT.CLIENT_PK;

drop table if exists CLIENT;

drop index if exists COMPANY.COMPANY_PK;

drop table if exists COMPANY;

drop index if exists CONTRACT.ASSOCIATION_1_FK;

drop index if exists CONTRACT.CONTRACT_PK;

drop table if exists CONTRACT;

drop index if exists PROSPECT.PROSPECT_PK;

drop table if exists PROSPECT;

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table CLIENT 
(
   ID                   integer                        not null,
   constraint PK_CLIENT primary key clustered (ID)
);

/*==============================================================*/
/* Index : CLIENT_PK                                            */
/*==============================================================*/
create unique clustered index CLIENT_PK on CLIENT (
ID ASC
);

/*==============================================================*/
/* Table : COMPANY                                              */
/*==============================================================*/
create table COMPANY 
(
   ID                   integer                        not null,
   constraint PK_COMPANY primary key clustered (ID)
);

/*==============================================================*/
/* Index : COMPANY_PK                                           */
/*==============================================================*/
create unique clustered index COMPANY_PK on COMPANY (
ID ASC
);

/*==============================================================*/
/* Table : CONTRACT                                             */
/*==============================================================*/
create table CONTRACT 
(
   IDCONTRACT           integer                        not null,
   ID                   integer                        not null,
   constraint PK_CONTRACT primary key clustered (IDCONTRACT)
);

/*==============================================================*/
/* Index : CONTRACT_PK                                          */
/*==============================================================*/
create unique clustered index CONTRACT_PK on CONTRACT (
IDCONTRACT ASC
);

/*==============================================================*/
/* Index : ASSOCIATION_1_FK                                     */
/*==============================================================*/
create index ASSOCIATION_1_FK on CONTRACT (
ID ASC
);

/*==============================================================*/
/* Table : PROSPECT                                             */
/*==============================================================*/
create table PROSPECT 
(
   ID                   integer                        not null,
   constraint PK_PROSPECT primary key clustered (ID)
);

/*==============================================================*/
/* Index : PROSPECT_PK                                          */
/*==============================================================*/
create unique clustered index PROSPECT_PK on PROSPECT (
ID ASC
);

alter table CLIENT
   add constraint FK_CLIENT_HERITAGE__COMPANY foreign key (ID)
      references COMPANY (ID)
      on update restrict
      on delete restrict;

alter table CONTRACT
   add constraint FK_CONTRACT_ASSOCIATI_CLIENT foreign key (ID)
      references CLIENT (ID)
      on update restrict
      on delete restrict;

alter table PROSPECT
   add constraint FK_PROSPECT_HERITAGE__COMPANY foreign key (ID)
      references COMPANY (ID)
      on update restrict
      on delete restrict;

